|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1			| C1, C2, C3, C4	| 4.7uF	| C_0805_2012Metric	| 4
| 2			| R1, R2	| 5.1k	| R_0805_2012Metric	| 2
| 3			| R3, R4	| 470	| R_0805_2012Metric	| 2
| 4			| R5	| 4.7k	| R_0805_2012Metric	| 1
| 5			| L1	| BLM21PG300SN1D	| L_0805_2012Metric	| 1
| 6			| D1	| TO-1608BC-MYF	| LED_0603_1608Metric	| 1
| 7			| D2	| TO-1608BC-PG	| LED_0603_1608Metric	| 1
| 8			| U2	| CP2104	| QFN-24-1EP_4x4mm_P0.5mm_EP2.6x2.6mm	| 1
| 9			| U1	| USBLC6-2SC6	| SOT-23-6	| 1
| 10		| J4, J5	| PLS-3	| PinHeader_1x03_P2.54mm_Vertical	| 2
| 11		| J1, J2	| PBS-3	| PinSocket_1x03_P2.54mm_Vertical	| 2
| 12		| J3	| HRO TYPE-C-31-M-12	| USB_C_Receptacle_HRO_TYPE-C-31-M-12	| 1
